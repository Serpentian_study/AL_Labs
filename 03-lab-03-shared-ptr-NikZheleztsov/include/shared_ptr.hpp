// Copyright 2021 NikZheleztsov <your_email>

#ifndef INCLUDE_SHARED_PTR_HPP_
#define INCLUDE_SHARED_PTR_HPP_
#include <cstddef>
#include <atomic>
#include <utility>

template <typename T>
class SharedPtr {
public:
    typedef T* Ptr;
    typedef std::atomic<unsigned int> Counter;
    typedef Counter* CounterPtr;

    // Constructors
    SharedPtr() noexcept;
    explicit SharedPtr(Ptr ptr);
    SharedPtr(const SharedPtr& other) noexcept;
    SharedPtr(SharedPtr&& other) noexcept;

    // Operaftor =
    auto operator=(const SharedPtr& r) -> SharedPtr&;
    auto operator=(SharedPtr&& r) -> SharedPtr&;

    // Destructor
    ~SharedPtr();

    // if there is an object
    operator bool() const;

    // The behavior is undefined if !ptr_ (according to cppref)
    auto operator*() const -> T&;
    auto operator->() const -> T*;

    auto get() -> Ptr;
    void reset();
    void reset(Ptr ptr);
    void swap(SharedPtr& r);
    // the number of SharedPtr objects
    auto use_count() const -> size_t;

private:
    void destroy() noexcept;

    Ptr ptr_;
    CounterPtr count_;
};

template class SharedPtr<int>;

#endif // INCLUDE_SHARED_PTR_HPP_
