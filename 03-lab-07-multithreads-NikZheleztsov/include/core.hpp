// Copyright 2021 Nikita Zheleztsov

#ifndef INCLUDE_CORE_HPP_
#define INCLUDE_CORE_HPP_

#include <atomic>
#include <memory>

#include "config.hpp"
#include "logger.hpp"
#include "workers.hpp"

class Core {
  std::unique_ptr<Logger> logger_ = nullptr;
  std::unique_ptr<WorkersPool> pool_ = nullptr;
  std::atomic<bool> shutdown_ = false;

 public:
  Core() = default;
  Core(const Core& other) = delete;
  Core& operator=(const Core& other) = delete;
  ~Core() = default;

  void Init() noexcept;
  void Run() noexcept;
  void Shutdown() noexcept;
};

#endif  // INCLUDE_CORE_HPP_
