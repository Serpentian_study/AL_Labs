#include "client.hpp"

int main(int argc, char* argv[]) {
  if (argc != 4) {
    std::cout << "Usage: http-client <host> <port> <target>";
    return EXIT_FAILURE;
  }

  const std::string host(argv[1]);
  const std::string port(argv[2]);
  const std::string target(argv[3]);

  Client client(host, port, target);
  auto init_status = client.Init();
  if (!init_status.ok()) {
    std::cout << init_status.what() << std::endl;
    return EXIT_FAILURE;
  }

  std::string input;
  std::string response;

  std::cout << "http_client > ";
  while (std::cin >> input) {
    {
      auto status = client.SendPostRequst(input);
      if (!status.ok()) {
        std::cout << status.what() << std::endl;
        break;
      }
    }

    {
      auto status = client.GetResponse(response);
      if (!status.ok()) {
        std::cout << status.what() << std::endl;
        break;
      }

      std::cout << "Recieved response from server: \n"
                << response << std::endl
                << "http_client > ";
    }
  }

  return EXIT_SUCCESS;
}
