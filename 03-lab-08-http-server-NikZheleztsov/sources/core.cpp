// Copyright 2021 NikZheleztsov <mail>

#include "core/core.hpp"

#include "network/suggest_service.hpp"
#include "utils/logger.hpp"

Core::Core(Config_cmd&& conf) noexcept
    : server_(new Server),
      machine_(new SuggestMachine(conf.file_name)),
      conf_(conf),
      exiting_(false) {}

Status Core::Init() noexcept {
  auto status = server_->Init(conf_.address_name, conf_.port);
  if (!status.ok()) {
    LOG_ERROR("Error while server initialization. Exiting...");
    Shutdown();
    return Status(Status::Code::FAILURE);
  }

  server_->RegisterService(std::make_unique<SuggestService>(*machine_.get()));
  return Status();
}

Status Core::Run() noexcept {
  auto status1 = machine_->Run();
  auto status2 = server_->Run();

  if (!status1.ok() || !status2.ok()) {
    LOG_ERROR("Exiting...");
    Shutdown();
    return Status(Status::Code::FAILURE);
  }

  return Status();
}

void Core::Shutdown() noexcept {
  if (!exiting_) {
    exiting_ = true;
    server_->Shutdown();
    machine_->Shutdown();
  }
}

Core::~Core() { Shutdown(); }
