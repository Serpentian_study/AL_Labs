// Copyright 2021 Nikita Zheleztsov

#ifndef INCLUDE_CORE_HPP_
#define INCLUDE_CORE_HPP_

#include <atomic>
#include <memory>

#include "ThreadPool.h"
#include "logger.hpp"

class Core {
  std::unique_ptr<Logger> logger_ = nullptr;
  std::unique_ptr<ThreadPool> producers_ = nullptr;
  std::unique_ptr<ThreadPool> consumers_ = nullptr;

  struct Config {
    std::string url;
    uint64_t depth;
    uint32_t network_threads;
    uint32_t parser_threads;
    std::string output_path;
    std::string log_path;
  };

  bool isOk() const noexcept;
  void produce(std::string& url, const uint64_t& depth,
               uint64_t& current_depth) noexcept;
  void consume(std::string& html, std::string& url) noexcept;

 public:
  Core() = default;
  bool Init() noexcept;
  Core(const Core& other) = delete;
  Core& operator=(const Core& other) = delete;
  ~Core() = default;

  void Run() noexcept;
  void Wait() noexcept;

  static Config config;
};

#endif  // INCLUDE_CORE_HPP_
