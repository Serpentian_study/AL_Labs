// Copyright 2021 NikZheleztsov <mail>

#ifndef STACK_2_H_
#define STACK_2_H_

#include <type_traits>

#include "vector/vector.h"

// 2st task
template <class T, class Container = Vector<T>>
class Stack_2 {
  static_assert(std::is_move_constructible_v<T>);
  static_assert(std::is_move_assignable_v<T>);

 public:
  typedef Container container_type;
  typedef typename Container::value_type value_type;
  typedef typename Container::size_type size_type;
  typedef typename Container::reference reference;
  typedef typename Container::const_reference const_reference;

  // Constructors
  explicit Stack_2(const Container& cont = Container()) : cont_(cont) {}
  Stack_2(Stack_2&& other) : cont_(std::move(other.cont_)){};
  Stack_2(const Stack_2&) = delete;

  // Operator =
  Stack_2& operator=(const Stack_2&) = delete;
  Stack_2& operator=(Stack_2&& other);

  // Destructor
  ~Stack_2() = default;

  // Interface
  size_type size() const { return cont_.size(); }
  template <typename... Args>
  void push_emplace(Args&&... value);
  void push(T&& value);
  const T& head() const { return cont_.back(); }
  T pop();

 private:
  Container cont_;
};

template <class T, class Container>
template <typename... Args>
void Stack_2<T, Container>::push_emplace(Args&&... args) {
  cont_.emplace_back(std::forward<Args>(args)...);
}

template class Stack_2<int>;

#endif  // STACK_2_H_
