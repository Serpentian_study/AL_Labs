// Copyright 2021 Nikita Zheleztsov

#include <fstream>
#include <iostream>

#include "./student.h"
#include "./table.h"
#include "nlohmann/json.hpp"

using json = nlohmann::json;

void parse_json(json& js, Table* table) {
  if (js["_meta"]["count"].get<size_t>() != js.at("items").size())
    throw std::invalid_argument("Wrong meta info");

  if (js.at("items").is_array()) {
    table->push_header({"name", "group", "avg", "debt"});

    for (auto const& stud : js.at("items")) {
      Student student(stud);
      student.push_to_table(*table);
    }

  } else {
    throw std::invalid_argument("\'items\' is not an array");
  }
}
