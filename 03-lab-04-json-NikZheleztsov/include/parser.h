#ifndef PARSER_H
#define PARSER_H

#include "table.h"
#include "nlohmann/json.hpp"
using json = nlohmann::json;

void parse_json(json& json, Table* table);

#endif // PARSER_H

