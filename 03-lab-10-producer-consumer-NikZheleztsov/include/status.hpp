// Copyright 2021 NikZheleztsov <mail>

#ifndef INCLUDE_STATUS_HPP_
#define INCLUDE_STATUS_HPP_

#include <string>

class Status {
 public:
  enum class Code { OK, UNIMPLEMENTED, FAILURE };

  Status() = default;
  explicit Status(Code code) noexcept : code_(code) {}
  explicit Status(const std::string &message,
                  Code code = Code::FAILURE) noexcept
      : code_(code), message_(message) {}
  ~Status() = default;

  Status &operator=(const Status &other) = default;

  Code code() const noexcept { return code_; }
  std::string what() const noexcept { return message_; }

  bool ok() const noexcept { return code_ == Code::OK; }
  bool is(const Status &other) const noexcept { return code_ == other.code_; }

  operator std::string() const noexcept { return message_; }
  operator const char *() const noexcept { return message_.c_str(); }

 private:
  Code code_ = Code::OK;
  std::string message_;
};

#endif  // INCLUDE_STATUS_HPP_
