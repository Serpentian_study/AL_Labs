// Copyright 2021 Nikita Zheleztsov

#include "workers.hpp"

#include <boost/log/trivial.hpp>
#include <iostream>

#include "PicoSHA2/picosha2.h"

WorkersPool::WorkersPool(Logger& log, uint8_t num)
    : shutdown_(false),
      active_threads_num_(0),
      threads_num_(num),
      logger_(log) {}

std::string WorkersPool::getTimerString() const noexcept {
  std::time_t now = Clock::to_time_t(Clock::now());
  std::string time_string(30, '\0');
  std::strftime(&time_string[0], time_string.size(), "%Y:%m:%d %H:%M:%S",
                std::localtime(&now));
  // cleaning
  auto pos = time_string.find('\0');
  time_string = time_string.substr(0, pos);
  return time_string;
}

void WorkersPool::worker() {
  ++active_threads_num_;
  const std::string needed("0000");

  while (!shutdown_) {
    std::string time = getTimerString();
    std::string rand = std::to_string(std::rand());

    std::string hash = picosha2::hash256_hex_string(rand);
    std::string for_check = hash.substr(hash.size() - needed.size());

    if (for_check == needed) {
      BOOST_LOG_TRIVIAL(info)
          << '[' << time << "] : data: \"" << rand << "\" hash: " << hash;
      logger_.Add(time, rand, hash);

    } else {
      BOOST_LOG_TRIVIAL(trace)
          << '[' << time << "] : data: \"" << rand << "\" hash: " << hash;
    }
  }

  --active_threads_num_;
  cv_.notify_all();
}

void WorkersPool::Run() noexcept {
  for (uint8_t i = 0; i < threads_num_; ++i) {
    std::thread(&WorkersPool::worker, this).detach();
  }
}

void WorkersPool::Shutdown() noexcept {
  if (!shutdown_) {
    std::unique_lock<std::mutex> lck(mutex_);
    shutdown_ = true;
    BOOST_LOG_TRIVIAL(info) << ": Waiting for pool to shut down...";
    while (active_threads_num_ > 0) {
      cv_.wait(lck);
    }

    BOOST_LOG_TRIVIAL(info) << ": The pool was shutted down successfully...";
  }
}
