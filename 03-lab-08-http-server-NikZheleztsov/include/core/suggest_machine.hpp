// Copyright 2021 NikZheleztsov <mail>

#ifndef INCLUDE_SUGGEST_MACHINE_HPP_
#define INCLUDE_SUGGEST_MACHINE_HPP_

#include <condition_variable>
#include <mutex>
#include <thread>
#include <unordered_map>

#include "server.pb.h"
#include "utils/status.hpp"

class SuggestMachine {
 public:
  typedef std::chrono::steady_clock::time_point TimePoint;
  typedef std::chrono::steady_clock Clock;
  typedef std::chrono::minutes TimeDuration;
  typedef std::unordered_multimap<std::string, Config::ConfOne>::const_iterator
      const_iter;

  explicit SuggestMachine(const std::string& name)
      : READ_AFTER_(15), CHECK_EVERY_(1), exiting_(false), file_name_(name) {}

  Status Run() noexcept;
  Response Query(const Request& req) noexcept;
  void Shutdown() noexcept;

 private:
  void readThreadMain() noexcept;
  Status read() noexcept;

  TimeDuration READ_AFTER_;
  TimeDuration CHECK_EVERY_;

  std::mutex mutex_;
  std::condition_variable cv_;
  std::thread read_thread_;
  bool exiting_;

  std::unordered_multimap<std::string, Config::ConfOne> log_;
  std::string file_name_;
};

#endif  // INCLUDE_SUGGEST_MACHINE_HPP_
