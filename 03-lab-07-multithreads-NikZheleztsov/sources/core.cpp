// Copyright 2021 Nikita Zheleztsov

#include "core.hpp"

void Core::Init() noexcept {
  logger_ = std::make_unique<Logger>(Config::GetInstance().GetLogDir(),
                                     Config::GetInstance().GetJsonPath());
  pool_ = std::make_unique<WorkersPool>(*logger_.get(),
                                        Config::GetInstance().GetThreadsNum());
}

void Core::Run() noexcept { pool_->Run(); }

void Core::Shutdown() noexcept {
  if (!shutdown_) {
    shutdown_ = true;
    pool_->Shutdown();
    logger_->Shutdown();
  }
}
