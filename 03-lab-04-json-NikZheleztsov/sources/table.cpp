// Copyright 2021 Nikita ZHeleztsov

#include "./table.h"

#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

void Table::print_hor_line(std::ostream& os) {
  os << '+';
  os << std::setw(m_width - 2) << std::setfill('-') << '-';
  os << '+' << std::endl;
}

void Table::push_header(std::vector<std::string> names,
                        std::vector<uint8_t> width) {
  if (width.size() == names.size() && width[0] != 0) {
    for (size_t i = 0; i < names.size(); ++i) {
      m_width += width[i] + 3;
      header.push_back(std::make_pair(names[i], width[i]));
    }

  } else {
    for (auto i = names.begin(); i != names.end(); ++i) {
      m_width += i->size() + 3;
      header.push_back(std::make_pair(*i, i->size()));
    }
  }
}

void Table::push_tuple(std::vector<std::string> tup) {
  if (header.empty() || tup.size() != header.size())
    throw std::invalid_argument("Error while printing table");

  for (size_t i = 0; i < tup.size(); ++i)
    if (tup[i].size() > header[i].second) {
      m_width = m_width - header[i].second + tup[i].size();
      header[i].second = tup[i].size();
    }

  tuples.push_back(tup);
}

std::ostream& operator<<(std::ostream& os, Table& tab) {
  // printing of header
  tab.print_hor_line(os);
  os << '|';
  for (auto x : tab.header) {
    os << ' ' << std::setfill(' ') << std::right << std::setw(x.second)
       << x.first << ' ' << '|';
  }
  os << std::endl;
  tab.print_hor_line(os);

  // tuples
  for (auto x : tab.tuples) {
    os << '|';

    for (size_t i = 0; i < x.size(); i++) {
      os << ' ' << std::setfill(' ') << std::left
         << std::setw(tab.header[i].second) << x[i] << " |";
    }

    os << std::endl;
  }

  tab.print_hor_line(os);

  return os;
}
