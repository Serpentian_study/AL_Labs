// Copyright 2021 Nikita Zheleztsov

#include "logger.hpp"

#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup.hpp>
#include <fstream>

void Logger::initBoostLogging() noexcept {
  boost::log::add_common_attributes();
  boost::log::register_simple_formatter_factory<
      boost::log::trivial::severity_level, char>("Severity");

  auto Console = boost::log::add_console_log(
      std::cout, boost::log::keywords::format =
                     "[%TimeStamp%][%Severity%][%ThreadID%]: %Message%");
}

Logger::Logger(const std::string& json_path) : json_path_(json_path) {
  json = std::make_unique<nlohmann::json>(nlohmann::json::array());
  initBoostLogging();
}

void Logger::InitDumpLog(const std::string& log_path) noexcept {
  boost::log::add_file_log(
      boost::log::keywords::file_name =
          std::string(log_path + "/crawler_log_%N.log"),
      // rotate every 10 MB
      boost::log::keywords::rotation_size = 100 * 1000 * 1000,
      boost::log::keywords::format =
          "[%TimeStamp][%Severity%][%ThreadID%]: %Message%");
}

void Logger::Add(std::string& url) noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  if (set_.find(url) == set_.end()) {
    BOOST_LOG_TRIVIAL(info) << "Found new picture: " << url;
    set_.insert(url);
  } else {
    // BOOST_LOG_TRIVIAL(info) << "Duplicate. Omitting...";
  }
}

void Logger::dump() noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  std::for_each(set_.begin(), set_.end(), [this](const std::string& url) {
    nlohmann::json temp = {url};
    json->push_back(std::move(temp));
  });

  std::ofstream out(json_path_, std::ios::out | std::ios::trunc);
  if (!out.is_open()) {
    BOOST_LOG_TRIVIAL(error) << "Unable to open file " << json_path_;
    return;
  }

  out << json->dump(2);
}

Logger::~Logger() { dump(); }
