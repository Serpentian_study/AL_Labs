#ifndef INCLDUE_UTILS_HPP
#define INCLDUE_UTILS_HPP

#include <gumbo.h>

#include <string>
#include <vector>

namespace utils {

std::string getHtml(std::string& url) noexcept;
std::string getHost(std::string& url) noexcept;
std::string getTarget(std::string& url) noexcept;

void search_for_links(GumboNode* node, std::vector<std::string>& vec) noexcept;
void search_for_pics(GumboNode* node, std::vector<std::string>& vec) noexcept;

}  // namespace utils

#endif  // INCLDUE_UTILS_HPP
