// Copyright 2021 NikZheleztsov <mail>

#ifndef INCLUDE_CORE_HPP_
#define INCLUDE_CORE_HPP_

#include <memory>
#include <string>

#include "core/suggest_machine.hpp"
#include "network/http_server.hpp"

struct Config_cmd {
  std::string address_name;
  std::string port;
  std::string file_name;
};

class Core {
 public:
  explicit Core(Config_cmd&& conf) noexcept;
  Status Init() noexcept;
  Status Run() noexcept;
  void Shutdown() noexcept;
  ~Core();

 private:
  std::unique_ptr<Server> server_;
  std::unique_ptr<SuggestMachine> machine_;
  Config_cmd conf_;
  bool exiting_;
};

#endif  // INCLUDE_CORE_HPP_
