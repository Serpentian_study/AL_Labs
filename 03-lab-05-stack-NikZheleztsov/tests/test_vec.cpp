// Copyright 2021 My Name <my_email>

#include <gtest/gtest.h>
#include "vector/vector.h"

// I'm pretty lazy to write all of this
TEST(vector, vec_constructors) {
  Vector v_0 {1, 2, 3, 4, 5};
  EXPECT_EQ(5, v_0.size());
  EXPECT_EQ(5, v_0.back());

  Vector v_1(v_0);
  EXPECT_EQ(v_1.size(), 5);

  Vector v_2(std::move(v_1));
  EXPECT_EQ(v_1.size(), 0);
}

TEST(vector, push_pop) {
  Vector v_0 {1, 2, 3, 4, 5};
  v_0.pop_back();
  v_0.push_back(100);
  EXPECT_EQ(v_0.size(), 5);
  EXPECT_EQ(v_0.back(), 100);
}
