// Copyright 2021 Nikita Zheleztsov

#include <fstream>
#include <iostream>

#include "./parser.h"
#include "./table.h"
#include "nlohmann/json.hpp"

using json = nlohmann::json;

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cout << R"(Invalid number of arguments. Please add path
to JSON file)" << std::endl;
    return 1;
  }

  std::ifstream json_file(argv[1]);

  if (json_file.is_open()) {
    json js;
    json_file >> js;

    // Using the heap rather than the stack due
    // to the fact that the table can be pretty large
    Table* table_of_students = new Table;
    try {
      parse_json(js, table_of_students);
    } catch (std::exception& exc) {
      std::cout << "Error while parsing of the file\n";
      std::cout << exc.what() << std::endl;
      return 2;
    }

    std::cout << *table_of_students;
    delete table_of_students;

  } else {
    std::cout << "Unable to open the file\n";
    return 1;
  }

  return 0;
}
