// Copyright 2021 Nikita Zheleztsov

#ifndef INCLUDE_LOGGER_HPP_
#define INCLUDE_LOGGER_HPP_

#include <boost/log/trivial.hpp>
#include <memory>
#include <mutex>
#include <nlohmann/json.hpp>
#include <set>
#include <string>

class Logger {
  std::mutex mutex_;
  std::string json_path_;
  std::unique_ptr<nlohmann::json> json;
  std::set<std::string> set_;

  uint64_t last_snapshot_ = 0;

  void initBoostLogging() noexcept;
  void dump() noexcept;

 public:
  Logger(const std::string& json_path);
  void InitDumpLog(const std::string& log_path) noexcept;
  void Add(std::string& url) noexcept;
  ~Logger();
};

#endif  // INCLUDE_LOGGER_HPP_
