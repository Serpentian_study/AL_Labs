// Copyright 2021 NikZheleztsov <your_email>

#include "shared_ptr.hpp"
#include <cassert>

template <typename T>
SharedPtr<T>::SharedPtr() noexcept : ptr_(nullptr), count_(nullptr) {}

template <typename T>
SharedPtr<T>::SharedPtr(SharedPtr::Ptr ptr)
    : ptr_(ptr), count_(new Counter(1)) {}

template <typename T>
SharedPtr<T>::SharedPtr(const SharedPtr<T>& other) noexcept
    : ptr_(other.ptr_), count_(other.count_) {
  ++(*count_);
}

template <typename T>
SharedPtr<T>::SharedPtr(SharedPtr&& other) noexcept
    : ptr_(other.ptr_), count_(other.count_) {
  other.ptr_ = nullptr;
  other.count_ = nullptr;
}

template <typename T>
auto SharedPtr<T>::operator=(const SharedPtr& r) -> SharedPtr& {
  if (ptr_) {
    delete ptr_;
    --(*count_);
  }

  ptr_ = r.ptr_;
  count_ = r.count_;
  ++(*count_);

  return *this;
}

template <typename T>
auto SharedPtr<T>::operator=(SharedPtr&& r) -> SharedPtr& {
  ptr_ = r.ptr_;
  count_ = r.count_;
  r.ptr_ = nullptr;
  r.count_ = nullptr;

  return *this;
}

template <typename T>
SharedPtr<T>::~SharedPtr<T>() {
    destroy();
}

template <typename T>
void SharedPtr<T>::destroy() noexcept {
  if (ptr_) {
    if (count_) {
      if (*count_ == 1) {
        delete ptr_;
        delete count_;

      } else {
        --(*count_);
      }
    } else {
       delete ptr_;
       // Smth is wrong if we are here
       assert(false);
    }

  } else if (count_) {
      // and here
      delete count_;
      assert(false);
  }

  ptr_ = nullptr;
  count_ = nullptr;
}

template <typename T>
SharedPtr<T>::operator bool() const {
    return ptr_;
}

template <typename T>
auto SharedPtr<T>::operator*() const -> T& {
    return *ptr_;
}

template <typename T>
auto SharedPtr<T>::operator->() const -> T* {
    return ptr_;
}

template <typename T>
auto SharedPtr<T>::get() -> Ptr {
    return ptr_;
}

template <typename T>
void SharedPtr<T>::reset() {
    destroy();
}

template <typename T>
void SharedPtr<T>::reset(SharedPtr::Ptr ptr) {
    destroy();
    assert(!ptr_);
    assert(!count_);

    count_ = new Counter(1);
    ptr_ = ptr;
}

template <typename T>
auto SharedPtr<T>::use_count() const -> size_t {
    if (!count_) {
        return 0;
    }

    return *count_;
}

template <typename T>
void SharedPtr<T>::swap(SharedPtr& r) {
    auto temp(std::move(*this));
    *this = std::move(r);
    r = std::move(temp);
}
