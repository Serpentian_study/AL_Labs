cmake_minimum_required(VERSION 3.18)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

option(BUILD_TESTS "Build tests" ON)

set(HUNTER_CACHE_SERVERS
    "https://github.com/bmstu-iu8-cpp-sem-3/hunter-cache"
    CACHE STRING "Default cache server")

include("cmake/HunterGate.cmake")

huntergate(URL "https://github.com/cpp-pm/hunter/archive/v0.23.314.tar.gz" SHA1
           "95c47c92f68edb091b5d6d18924baabe02a6962a")

project(builder VERSION 0.1.0)
string(APPEND CMAKE_CXX_FLAGS " -pedantic -Werror -Wall -Wextra")
string(APPEND CMAKE_CXX_FLAGS " -Wshadow -Wnon-virtual-dtor")
string(APPEND CMAKE_CXX_FLAGS " -g")

hunter_add_package(Async++)
hunter_add_package(BoostProcess)
find_package(GTest CONFIG REQUIRED)
find_package(Async++ CONFIG REQUIRED)
find_package(BoostProcess CONFIG REQUIRED)

add_library(${PROJECT_NAME}_LIB STATIC
            ${CMAKE_CURRENT_SOURCE_DIR}/sources/builder.cpp)

add_library(Logger STATIC ${CMAKE_CURRENT_SOURCE_DIR}/sources/logger.cpp)

add_executable(${PROJECT_NAME} ${CMAKE_CURRENT_SOURCE_DIR}/cmd/main.cpp)

include_directories("$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>"
                    "$<INSTALL_INTERFACE:include>")

target_link_libraries(${PROJECT_NAME}_LIB Logger Async++::Async++
                      BoostProcess::boost_process)
target_link_libraries(${PROJECT_NAME} Logger ${PROJECT_NAME}_LIB)

if(BUILD_TESTS)
  add_executable(tests ${CMAKE_CURRENT_SOURCE_DIR}/tests/example.cpp)
  target_link_libraries(tests GTest::gtest_main)
  enable_testing()
  add_test(NAME unit_tests COMMAND tests)
endif()

include(CPackConfig.cmake)

install(
  TARGETS ${PROJECT_NAME}
  EXPORT "${PROJECT_NAME}-targets"
  RUNTIME DESTINATION bin
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  INCLUDES
  DESTINATION include)

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include/ DESTINATION include)

install(
  EXPORT "${PROJECT_NAME}-targets"
  NAMESPACE "${PROJECT_NAME}::"
  DESTINATION "lib/cmake")
