// Copyright 2021 Nikita Zheleztsov

#include "core.hpp"

#include <gumbo.h>

#include "utils.hpp"

Core::Config Core::config;

bool Core::Init() noexcept {
  if (isOk()) {
    logger_ = std::make_unique<Logger>(config.output_path);
    producers_ = std::make_unique<ThreadPool>(config.network_threads);
    consumers_ = std::make_unique<ThreadPool>(config.parser_threads);

    if (!config.log_path.empty()) {
      logger_->InitDumpLog(config.log_path);
    }

    return true;

  } else {
    return false;
  }
}

bool Core::isOk() const noexcept {
  return (!config.url.empty() && !config.output_path.empty() && config.depth &&
          config.network_threads && config.parser_threads);
}

void Core::Run() noexcept {
  uint64_t depth = 0;
  producers_->enqueue(&Core::produce, this, config.url, config.depth, depth);
}

void Core::produce(std::string& url, const uint64_t& depth,
                   uint64_t& current_depth) noexcept {
  ++current_depth;
  if (current_depth > depth) {
    return;
  }

  BOOST_LOG_TRIVIAL(info) << "PRODUCER: Parsing " << url;
  auto html = utils::getHtml(url);
  this->consumers_->enqueue(&Core::consume, this, html, url);

  // if needed enqueue new urls to producers
  if (depth > current_depth) {
    std::vector<std::string> all_found_links;
    GumboOutput* output = gumbo_parse(html.c_str());
    utils::search_for_links(output->root, all_found_links);
    gumbo_destroy_output(&kGumboDefaultOptions, output);

    std::for_each(all_found_links.begin() + 1, all_found_links.end(),
                  [this, &current_depth, &url](std::string& url_) {
                    if (url_[0] == '/') url_ = utils::getHost(url) + url_;
                    producers_->enqueue(&Core::produce, this, url_,
                                        config.depth, current_depth);
                  });
  }
}

void Core::consume(std::string& html, std::string& url) noexcept {
  BOOST_LOG_TRIVIAL(info) << "PRODUCER: Parsing";
  std::vector<std::string> all_found_pics;
  GumboOutput* output = gumbo_parse(html.c_str());
  utils::search_for_pics(output->root, all_found_pics);
  gumbo_destroy_output(&kGumboDefaultOptions, output);

  std::for_each(all_found_pics.begin(), all_found_pics.end(),
                [this, &url](std::string& url_) {
                  // Post process
                  if (url_[0] == '/')
                    url_ = "https://" + utils::getHost(url) + url_;

                  // Enqueue
                  logger_->Add(url_);
                });
}

void Core::Wait() noexcept {
  producers_->Run();
  while (producers_->is_working() || consumers_->is_working()) {
    std::this_thread::sleep_for(std::chrono::seconds(2));
  }

  std::this_thread::sleep_for(std::chrono::seconds(2));
  BOOST_LOG_TRIVIAL(info) << "Exiting...";
}
