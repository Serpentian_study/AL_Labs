// Copyright 2021 Nikita Zheleztsov

#include "logger.hpp"

#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup.hpp>
#include <fstream>

void Logger::initBoost() noexcept {
  boost::log::add_common_attributes();

  boost::log::register_simple_formatter_factory<
      boost::log::trivial::severity_level, char>("Severity");

  auto FileLog = boost::log::add_file_log(
      boost::log::keywords::file_name =
          std::string(log_dir_path_ + "/log_%N.log"),
      // rotate every 10 MB
      boost::log::keywords::rotation_size = 10 * 1000 * 1000,
      // midnight rotation
      boost::log::keywords::time_based_rotation =
          boost::log::sinks::file::rotation_at_time_point{0, 0, 0},
      boost::log::keywords::format = "[%Severity%][%ThreadID%]%Message%");

  auto ConsoleLog = boost::log::add_console_log(
      std::cout,
      boost::log::keywords::format = "[%Severity%][%ThreadID%]%Message%");
}

Logger::Logger(const std::string& log_path, const std::string& json_path)
    : log_dir_path_(log_path), json_path_(json_path) {
  json = std::make_unique<nlohmann::json>(nlohmann::json::array());
  initBoost();
}

void Logger::Add(std::string& time, std::string& data,
                 std::string hash) noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  nlohmann::json temp = {{"timestamp", time}, {"hash", hash}, {"data", data}};
  json->push_back(std::move(temp));
}

void Logger::Shutdown() noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  std::ofstream out(json_path_, std::ios::out | std::ios::trunc);
  if (!out.is_open()) {
    BOOST_LOG_TRIVIAL(error) << "Unable to open file " << json_path_;
    return;
  }

  out << json->dump(2);
}
