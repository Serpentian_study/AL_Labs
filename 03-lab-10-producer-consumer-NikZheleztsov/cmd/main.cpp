#include <getopt.h>

#include <cstring>
#include <iostream>

#include "core.hpp"
#include "status.hpp"

void help() {
  printf("Usage: crawler [OPTIONS]\n");
  printf(
      "Mandatory arguments to long options are mandatory "
      "for short options too.\n");
  printf("  -h, --help                      display this help and exit\n");
  printf("  -u, --url <URL>                 <URL> to parse\n");
  printf("  -d, --depth N                   how deep to go\n");
  printf(
      "  -n, --network_threads N         number of threads for url "
      "downloading\n");
  printf(
      "  -p, --parser_threads N          number of threads for pic parsing\n");
  printf("  -o, --output <FILE>             where to write found pics\n");
  printf("  -l, --log_dir <DIR>             if and where to write logs\n");
}

Status parse_command_line_arguments(int argc, char **argv) {
  auto failure = Status(Status::Code::FAILURE);

  std::string url, output("file.json"), log_dir;
  uint32_t netw_t(5), parser_t(5);
  uint64_t depth(1);

  int c;
  while (true) {
    static struct option long_options[] = {
        {"url", required_argument, 0, 'u'},
        {"depth", required_argument, 0, 'd'},
        {"network_threads", required_argument, 0, 'n'},
        {"parser_threads", required_argument, 0, 'p'},
        {"output", required_argument, 0, 'o'},
        {"log_dir", required_argument, 0, 'l'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}};

    int option_index = 0;
    c = getopt_long(argc, argv, "u:d:n:p:o:l:h", long_options, &option_index);

    if (c == -1) {
      break;
    }

    switch (c) {
      case 'u':
        url = optarg;
        break;

      case 'd':
        depth = std::stoull(optarg);
        break;

      case 'n':
        netw_t = std::stoul(optarg);
        break;

      case 'p':
        parser_t = std::stoul(optarg);
        break;

      case 'o':
        output = optarg;
        break;

      case 'l':
        log_dir = optarg;
        break;

      case 'h':
        help();
        return failure;

      default:
        return failure;
    }
  }

  Core::config.url = url;
  Core::config.depth = depth;
  Core::config.network_threads = netw_t;
  Core::config.parser_threads = parser_t;
  Core::config.output_path = output;
  Core::config.log_path = log_dir;

  return Status();
}

int main(int argc, char *argv[]) {
  if (!parse_command_line_arguments(argc, argv).ok()) {
    return EXIT_FAILURE;
  }

  Core core;
  if (!core.Init()) {
    std::cout << "The program wasn't initialized. Exiting...";
    return EXIT_FAILURE;
  }

  core.Run();
  core.Wait();

  return EXIT_SUCCESS;
}
