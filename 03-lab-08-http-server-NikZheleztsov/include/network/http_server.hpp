// Copyright 2021 NikZheleztsov <mail>

#ifndef INCLUDE_HTTP_SERVER_HPP_
#define INCLUDE_HTTP_SERVER_HPP_

#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast.hpp>
#include <boost/beast/http.hpp>
#include <memory>
#include <string>
#include <thread>

#include "network/service.hpp"
#include "utils/status.hpp"

namespace beast = boost::beast;
namespace http = beast::http;
namespace net = boost::asio;
using tcp = boost::asio::ip::tcp;

class Server {
 public:
  Server() noexcept;

  // non-copyable
  Server(const Server& other) = delete;
  Server& operator=(const Server& other) = delete;

  // Interface
  void RegisterService(std::unique_ptr<Service>&& service) noexcept;
  Status Init(const std::string& address, const std::string& port) noexcept;
  Status Run();
  void Shutdown();

 private:
  template <class Stream>
  struct send_lambda {
    Stream& stream_;
    bool& close_;
    beast::error_code& ec_;

    explicit send_lambda(Stream& stream, bool& close, beast::error_code& ec)
        : stream_(stream), close_(close), ec_(ec) {}

    template <bool isRequest, class Body, class Fields>
    void operator()(http::message<isRequest, Body, Fields>&& msg) const {
      // Determine if we should close the connection after
      close_ = msg.need_eof();

      // We need the serializer here because the serializer requires
      // a non-const file_body, and the message oriented version of
      // http::write only works with const messages.
      http::serializer<isRequest, Body, Fields> sr{msg};
      http::write(stream_, sr, ec_);
    }
  };

  void session(tcp::socket& socket) noexcept;
  void launch_and_wait() noexcept;
  bool isOk() const noexcept;

  net::ip::address address_;
  unsigned short port_;
  net::io_context ioc_;

  bool init_;
  bool exiting_;
  std::thread thread_;

  std::unique_ptr<Service> service_;
};

#endif  // INCLUDE_HTTP_SERVER_HPP_
